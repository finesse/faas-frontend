import React from "react";
import moment from "moment";

/**
 * Abstract job view component.
 * 
 * This loads the job ID specified by the router and is intended to be extended.
 */
class AbstractJobView extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            error: null,
            isLoaded: false, // For when a job has been fetched via API, not necessarily finished.
            finishChecksExhausted: false,
            item: null,
        };
    }

    componentDidMount() {
        const { uuid } = this.props.match.params

        const jobUrl = `${ process.env.REACT_APP_API_URL }/job/${ uuid }`;

        const awaitFinishedJob = ( retries = process.env.REACT_APP_BACKOFF_RETRIES, delay = process.env.REACT_APP_BACKOFF_DELAY ) => {
            const sleep = ( time ) => {
                return new Promise( ( resolve ) => setTimeout( resolve, time ) );
            }

            return new Promise( ( resolve, reject ) => {
                const doFetch = ( retries, delay ) => {
                    fetch( jobUrl ).then(
                        response => response.json()
                    ).then(
                        ( response ) => {
                            let item = new Job( response );

                            // Update state with latest item.
                            this.setState(
                                {
                                    item,
                                }
                            );

                            if ( item.isFinished() ) {
                                resolve( item );
                            } else {
                                if ( retries <= 1 ) {
                                    return reject();
                                }
    
                                sleep( delay * 2 ).then( () => {
                                    doFetch( retries - 1, delay * 2 );
                                } );
                            }
                        }
                    );
                }
    
                doFetch( retries, delay );
            } );
        }

        fetch(
            jobUrl
        ).then(
            response => response.json()
        ).then(
            ( response ) => {
                const item = new Job( response );

                this.setState(
                    {
                        isLoaded: true,
                        item,
                    }
                );

                if ( item.isRunning() ) {
                    // Loop until job finishes or retries exceeded.
                    awaitFinishedJob().then(
                        ( item ) => {
                            this.setState(
                                {
                                    item,
                                }
                            );
                        }
                    ).catch(
                        () => {
                            this.setState(
                                {
                                    finishChecksExhausted: true,
                                }
                            );
                        }
                    )
                }
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            ( error ) => {
                this.setState(
                    {
                        isLoaded: true,
                        error
                    }
                );
            }
        );
    }
}

class Job {
    constructor( data ) {
        this.uuid = data.uuid;
        this.status = data.status;
        this.title = data.title;
        this.description = data.description;

        if ( [ "finished", "failed" ].includes( data.status ) ) {
            this.started = moment( data.start_time );
            this.finished = moment( data.finish_date );
            this.duration = moment.duration( data.run_time );
            this.inputKatUrl = data.routes.outputs.data.inputkat;
        }

        if ( "finished" === data.status ) {
            this.finesseVersion = data.finesse_version;
            this.finesseVersionFull = data.finesse_version_full;
            this.katUrl = data.routes.outputs.data.kat;
            this.normKatUrl = data.routes.outputs.data.normkat;
            this.outUrl = data.routes.outputs.data.out;
            this.stdoutUrl = data.routes.outputs.data.stdout;
            this.plots = data.routes.outputs.plots.formats;
        }

        if ( "failed" === data.status ) {
            this.errorMsg = data.error_msg;
        }
    }

    isQueued() {
        return "queued" === this.status;
    }

    isStarted() {
        return "started" === this.status;
    }

    isRunning() {
        return this.isQueued() || this.isStarted();
    }

    isFinished() {
        return [ "finished", "failed" ].includes( this.status );
    }

    finishedDate() {
        return this.isFinished() ? this.finished.format() : null;
    }

    humanFinishedDate() {
        return this.isFinished() ? this.finished.fromNow() : null;
    }

    humanDuration() {
        return this.isFinished() ? `${ this.duration.milliseconds().toFixed( process.env.REACT_APP_MAX_RUN_TIME_DIGITS ) }s` : null;
    }
}

class InterstitialErrorBlock extends React.Component {
    render() {
        const { error } = this.props;

        let msg;

        if ( error instanceof Error ) {
            msg = error.message;
        } else {
            msg = error;
        }

        return (
            <div className="text-center">
                <p className="d-block lead text-danger">{ msg }</p>
            </div>
        );
    }
}

class InterstitialMessageBlock extends React.Component {
    render() {
        const { msg, spinner = false } = this.props;

        return (
            <div className="text-center">
                <p className="d-block lead">{ msg }</p>
                { spinner ? ( <div className="spinner-border text-secondary" role="status"></div> ) : null }
            </div>
        );
    }
}

const jobSuccessClasses = {
    queued: 'primary',
    started: 'primary',
    deferred: 'warning',
    finished: 'success',
    failed: 'danger',
};

export { AbstractJobView, Job, InterstitialErrorBlock, InterstitialMessageBlock, jobSuccessClasses };