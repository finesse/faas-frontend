import React from "react";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";
import queryString from "query-string";
import { Job, InterstitialErrorBlock, jobSuccessClasses } from "./util";

class Pagination extends React.Component {
    render() {
        return (
            <ReactPaginate
                previousLabel={ '«' }
                nextLabel={ '»' }
                breakLabel={ '...' }
                containerClassName={ 'pagination' }
                subContainerClassName={ 'pages pagination' }
                pageClassName={ 'page-item' }
                pageLinkClassName={ 'page-link' }
                previousClassName={ 'page-item' }
                previousLinkClassName={ 'page-link' }
                nextClassName={ 'page-item' }
                nextLinkClassName={ 'page-link' }
                breakClassName={ 'page-item' }
                breakLinkClassName={ 'page-link' }
                activeClassName={ 'active' }
                disabledClassName={ 'disabled' }
                { ...this.props }
            />
        );
    }
}

class JobsTableView extends React.Component {
    constructor( props ) {
        super( props );

        let page;

        if ( props.match && props.match.params && props.match.params.page ) {
            page = parseInt( props.match.params.page );
        }

        if ( ! page ) {
            page = 1;
        }

        const query = queryString.parse( this.props.location.search );

        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            page: 1,
            pageCount: 1, // Default for loading state.
            perPage: null,
            searchQuery: query.search || "", // Empty string needed for controlled text input.
        };

        // Properly resolve "this" in these methods.
        this.loadJobs = this.loadJobs.bind( this );
        this.handlePageClick = this.handlePageClick.bind( this );
        this.handleSearch = this.handleSearch.bind( this );
        this.handleSearchTextChange = this.handleSearchTextChange.bind( this );
    }

    componentDidMount() {
        this.loadJobs();
    }

    loadJobs() {
        const { page, searchQuery } = this.state;

        let extraParams = `/${ page }`;

        let url;

        if ( searchQuery ) {
            url = `${ process.env.REACT_APP_API_URL }/jobs/search/${ searchQuery }${ extraParams }`;
        } else {
            url = `${ process.env.REACT_APP_API_URL }/jobs${ extraParams }`;
        }

        fetch( 
            url
        ).then(
            response => response.json()
        ).then(
            ( response ) => {
                this.setState(
                    {
                        isLoaded: true,
                        items: response.jobs.map(
                            ( item ) => {
                                return new Job( item );
                            }
                        ),
                        page,
                        pageCount: parseInt( response.pages ),
                        perPage: parseInt( response.per_page ),
                    }
                );
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            ( error ) => {
                this.setState(
                    {
                        isLoaded: true,
                        error
                    }
                );
            }
        );
    }

    updateUrl() {
        const { page, searchQuery } = this.state;

        let url = '/scripts';

        if ( page ) {
            url += `/${ page }`;
        }

        if ( searchQuery ) {
            url += `?search=${ searchQuery }`;
        }

        this.props.history.push( url );
    }

    handlePageClick( data ) {
        const page = data.selected + 1;
    
        this.setState(
            {
                page,
            },
            () => {
                this.loadJobs();
                this.updateUrl();
            }
        );
    };

    handleSearch( event ) {
        event.preventDefault();

        this.loadJobs();
        this.updateUrl();
    }

    handleSearchTextChange( event ) {
        this.setState(
            {
                page: 1,
                searchQuery: event.target.value,
            }
        );
    }

    render() {
        const { error, isLoaded, items, page, pageCount, searchQuery } = this.state;

        let rows;
        
        if ( isLoaded ) {
            rows = items.map(
                ( item, index ) => {
                    return (
                        <tr key={ index }>
                            <td className="align-middle">
                                <span title={ item.finishedDate() }>{ item.humanFinishedDate() || 'n/a' }</span>
                            </td>
                            <td className="align-middle">
                                <span>{ item.title || '' }</span>
                            </td>
                            <td className="align-middle">
                                <div className={ `badge badge-${ jobSuccessClasses[ item.status ] }` }>{ item.status }</div>
                            </td>
                            <td className="align-middle">
                                <span>{ item.humanDuration() || 'n/a' }</span>
                            </td>
                            <td className="align-middle">
                                <span>{ item.finesseVersionFull || 'n/a' }</span>
                            </td>
                            <td>
                                <div className="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                    <Link to={ `/scripts/view/${ item.uuid }` } className="btn btn-primary" role="button">View</Link>
                                    <Link to={ `/scripts/new?from=${ item.uuid }` } className="btn btn-secondary" role="button">Clone</Link>
                                </div>
                            </td>
                        </tr>
                    );
                }
            );
        }

        const pagination = (
            <Pagination
                forcePage={ page - 1 } // Actually offset, not page.
                pageCount={ pageCount }
                onPageChange={ this.handlePageClick }
                pageRangeDisplayed={ 5 }
                hrefBuilder={ ( thisPage ) => `/scripts/${ thisPage }` }
            />
        );

        let content;

        if ( error ) {
            let msg = error;

            if ( error instanceof TypeError ) {
                // Override NetworkError message.
                msg = 'The job server cannot be reached right now. Please try again later.';
            }

            content = <InterstitialErrorBlock error={ msg }/>;
        } else {
            content = (
                <>
                    <div className="row">
                        <div className="col">{ pagination }</div>
                        <div className="col">
                            <form className="form-inline float-right" onSubmit={ this.handleSearch }>
                                <label className="sr-only" htmlFor="job-search-query">Search</label>
                                <input
                                    id="job-search-query"
                                    className="form-control"
                                    type="text"
                                    name="search"
                                    value={ searchQuery }
                                    onChange={ this.handleSearchTextChange }
                                    placeholder="Search"
                                    aria-label="Search"
                                />
                            </form>
                        </div>
                    </div>
                    <div className="table-responsive">
                        <table id="jobs-table" className="table table-striped table-sm">
                            <colgroup>
                                <col className="jobs-table-date-col"/>
                                <col/>
                                <col className="jobs-table-status-col"/>
                                <col className="jobs-table-runtime-col"/>
                                <col className="jobs-table-version-col"/>
                                <col className="jobs-table-option-col"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Run Time</th>
                                    <th scope="col">Finesse Version</th>
                                    <th scope="col">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                { ! Array.isArray( rows ) ? (
                                    <tr>
                                        <td colSpan="6">
                                            <div className="text-center">
                                                <div className="spinner-border text-secondary" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                ) : (
                                    ! rows.length ? (
                                        <tr>
                                            <td colSpan="6">No runs found.</td>
                                        </tr>
                                    ) : (
                                        rows
                                    )
                                ) }
                            </tbody>
                        </table>
                    </div>
                    { pagination }
                </>
            );
        }

        return (
            <>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
                    <h1 className="h2">Recent Scripts</h1>
                    <div className="btn-toolbar mb-2 mb-md-0">
                        <div className="btn-group mr-2" role="group">
                            <Link to="/scripts/new" className="btn btn-secondary" role="button">New</Link>
                        </div>
                    </div>
                </div>
                { content }
            </>
        );
    }
}

export default JobsTableView;