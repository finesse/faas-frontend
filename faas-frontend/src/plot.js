import React from "react";

class ScriptPlot extends React.Component {
    render() {
        const { item, format } = this.props;

        if ( "failed" === item.status ) {
            return <p>Job failed to run. Pykat reported: "{ item.errorMsg }".</p>
        }

        if ( ! ( format in item.plots ) ) {
            return <p>Invalid image format.</p>;
        }

        return (
            <img className="img-fluid" alt={ `Plot of script ${ item.uuid }` } src={ item.plots[ format ] }/>
        );
    }
}

export default ScriptPlot;