import React from "react";
import { Link } from "react-router-dom";
import { ScriptEditor } from './editor';
import ScriptPlot from './plot';
import { AbstractJobView, InterstitialErrorBlock, InterstitialMessageBlock, jobSuccessClasses } from './util';

class JobButtons extends React.Component {
    render() {
        const { item } = this.props;

        return (
            <div className="btn-toolbar mb-2 mb-md-0">
                <div className="btn-group mr-2" role="group">
                    <Link to={ `/scripts/new?from=${ item.uuid }` } className="btn btn-secondary" role="button">Clone</Link>
                    <button className="btn btn-secondary dropdown-toggle" id="script-download-dropdown-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        View
                    </button>
                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="script-download-dropdown-button">
                        <a href={ item.inputKatUrl } className="dropdown-item">Input file (.kat)</a>
                        { item.katUrl ? (
                            <a href={ item.katUrl } className="dropdown-item">Input file (.kat, parsed)</a>
                        ) : null }
                        { item.normKatUrl ? (
                            <a href={ item.normKatUrl } className="dropdown-item">Input file (.kat, normalised)</a>
                        ) : null }
                        { ( item.outUrl || item.stdoutUrl ) ? (
                            <div className="dropdown-divider"></div>
                        ) : null }
                        { item.outUrl ? (
                            <a href={ item.outUrl } className="dropdown-item">Output data</a>
                        ) : null }
                        { item.stdoutUrl ? (
                            <a href={ item.stdoutUrl } className="dropdown-item">Program output</a>
                        ) : null }
                    </div>
                </div>
            </div>
        );
    }
}

class JobResults extends AbstractJobView {
    render() {
        const { error, isLoaded, finishChecksExhausted, item } = this.state;

        let jobButtons;
        let content;
        let title;
        let description;

        if ( error ) {
            let msg = error;

            if ( error instanceof TypeError ) {
                // Override NetworkError message.
                msg = 'The job server cannot be reached right now. Please try again later.';
            }

            content = <InterstitialErrorBlock error={ msg }/>;
        } else if ( ! isLoaded ) {
            content = <InterstitialMessageBlock message="Loading..." spinner/>;
        } else if ( finishChecksExhausted ) {
            content = <InterstitialMessageBlock message="Job is still running. Refresh page to retry."/>;
        } else {
            title = item.title;
            description = item.description;

            if ( item.isRunning() ) {
                let message;

                if ( item.isQueued() ) {
                    message = "Queued...";
                } else {
                    message = "Running...";
                }

                content = <InterstitialMessageBlock message={ message } spinner/>;
            } else {
                const scriptInfo = (
                    <div className="table-responsive">
                        <table className="table table-sm">
                            <tbody>
                                <tr>
                                    <th className="align-middle" scope="row">
                                        <span>Status</span>
                                    </th>
                                    <td className="align-middle">
                                        <div className={ `badge badge-${ jobSuccessClasses[ item.status ] }` }>{ item.status }</div>
                                    </td>
                                </tr>
                                <tr>
                                    <th className="align-middle" scope="row">
                                        <span>Date Finished</span>
                                    </th>
                                    <td className="align-middle">
                                        <span title={ item.finishedDate() }>{ item.humanFinishedDate() || 'n/a' }</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th className="align-middle" scope="row">
                                        <span>Run Time</span>
                                    </th>
                                    <td className="align-middle">
                                        <span>{ item.humanDuration() || 'n/a' }</span>
                                    </td>
                                </tr>
                                <tr>
                                    <th className="align-middle" scope="row">
                                        <span>Finesse Version</span>
                                    </th>
                                    <td className="align-middle">
                                        <span>{ item.finesseVersionFull || 'n/a' }</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                );

                content = (
                    <div className="p-3">
                        { scriptInfo }
                        <h3>Plot</h3>
                        <ScriptPlot item={ item } format="svg" />
                        <h3>Script</h3>
                        <ScriptEditor item={ item } readOnly />
                    </div>
                );

                jobButtons = <JobButtons item={ item }/>;
            }
        }

        if ( description ) {
            // Convert newlines to breaks.
            // eslint-disable-next-line
            description = description.trim().replace(new RegExp("\r\n", "g"), "\n").split("\n").map(
                ( sentence, i ) =>
                    sentence ? <p key={ i } className="text-muted">{ sentence }</p> : null
            );
        }

        return (
            <>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
                    { ( title ) ? (
                        <h1 className="h2">{ title }</h1>
                    ) : (
                        <h1 className="h2 text-muted"><em>No title</em></h1>
                    ) }
                    { jobButtons }
                </div>
                { description }
                { content }
            </>
        );
    }
}

export default JobResults;