import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink,
    Redirect,
} from "react-router-dom";

import { version, homepage } from '../package.json';
import JobsTableView from "./jobs-view";
import JobResults from "./job-view";
import NewJobView from "./new-job-view";

class Site extends React.Component {
    render() {
        return (
            <>
                <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
                    <Link to="/" className="navbar-brand col-sm-3 col-md-2 mr-0">Finesse Hub <small>{ version }</small></Link>
                </nav>
                <div className="container-fluid">
                    <div className="row">
                        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                            <div className="sidebar-sticky">
                                <ul className="nav flex-column">
                                    <li className="nav-item">
                                        <NavLink to="/scripts" className="nav-link" activeClassName="active">
                                            Scripts
                                        </NavLink>
                                    </li>
                                </ul>
                                <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                                    <span>Help</span>
                                    <a className="d-flex align-items-center text-muted" href="/" aria-label="Add a new report">
                                        <span data-feather="plus-circle" />
                                    </a>
                                </h6>
                                <ul className="nav flex-column mb-2">
                                    <li className="nav-item">
                                        <a className="nav-link" href="http://www.gwoptics.org/finesse/reference/">
                                            Finesse Syntax
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="http://www.gwoptics.org/finesse/reference/cheatsheet.php">
                                            Finesse Cheat Sheet
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                            <Switch>
                                <Route path="/scripts/new" component={ NewJobView }/>
                                <Route path="/scripts/view/:uuid" component={ JobResults }/>
                                <Route path="/scripts/:page" component={ JobsTableView }/>
                                <Route path="/scripts/" component={ JobsTableView }/>
                                <Route path="/scripts" component={ JobsTableView }/>
                                <Redirect exact from="/" to="/scripts" />
                            </Switch>
                        </main>
                    </div>
                </div>
            </>
        );
    }
}

export default function App() {
    const home = document.createElement('a');
    home.href = homepage;

    return (
        <Router basename={ home.pathname }>
            <Site/>
        </Router>
    );
}