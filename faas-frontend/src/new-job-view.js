import React from "react";
import { Redirect } from 'react-router-dom'
import queryString from 'query-string'
import { ScriptEditor } from './editor';
import { Job, InterstitialMessageBlock } from './util';

class NewJobView extends React.Component {
    constructor( props ) {
        super( props );

        this.state = {
            script: null,
            error: null,
            isSubmitting: false,
            newJob: null,
            isLoadingCloneItem: false,
            cloneItem: null,
            cloneError: false,
        };

        // Properly resolve "this" in these methods.
        this.handleSubmit = this.handleSubmit.bind( this );
        this.onScriptChange = this.onScriptChange.bind( this );
    }
    
    componentDidMount() {
        const query = queryString.parse(this.props.location.search)

        if ( "from" in query ) {
            this.setState(
                {
                    isLoadingCloneItem: true,
                }
            );

            const jobUrl = `${ process.env.REACT_APP_API_URL }/job/${ query.from }`;

            fetch(
                jobUrl
            ).then(
                response => response.json()
            ).then(
                ( response ) => {
                    const cloneItem = new Job( response );

                    if ( ! cloneItem.isFinished() ) {
                        this.setState(
                            {
                                isLoadingCloneItem: false,
                                cloneError: new Error(`Job is not finished (${ cloneItem.status }).`),
                            }
                        );
                    } else {    
                        this.setState(
                            {
                                isLoadingCloneItem: false,
                                cloneItem,
                            }
                        );
                    }
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                ( error ) => {
                    this.setState(
                        {
                            isLoadingCloneItem: false,
                            cloneError: error,
                        }
                    );
                }
            );
        }
    }

    handleSubmit( event ) {
        const { script } = this.state;

        event.preventDefault();

        const submittedData = new FormData( event.target );
        const postData = new FormData();

        this.setState(
            {
                isSubmitting: true,
            }
        );

        postData.set( 'title', submittedData.get( 'script-title' ) );
        postData.set( 'description', submittedData.get( 'script-description' ) );
        postData.set( 'script', script );

        fetch(
            `${ process.env.REACT_APP_API_URL }/run`,
            {
                method: 'POST',
                body: postData,
            },
        ).then(
            response => response.json()
        ).then(
            ( response ) => {
                this.setState(
                    {
                        isSubmitting: false,
                        newJob: response.job.uuid,
                    }
                );
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            ( error ) => {
                this.setState(
                    {
                        isSubmitting: false,
                        error
                    }
                );
            }
        );
    }

    onScriptChange( value ) {
        this.setState(
            {
                script: value,
            }
        );
    }

    render() {
        const { error, isLoadingCloneItem, cloneError, cloneItem, isSubmitting, newJob } = this.state;

        if ( newJob ) {
            return <Redirect push to={ `/scripts/view/${ newJob }`} />
        }

        let message;

        if ( error ) {
            // Show error but continue to display form.
            message = <InterstitialMessageBlock message={ `Error: ${ error.message }` }/>;
        } else if ( cloneError ) {
            // Show only the error.
            return <InterstitialMessageBlock message={ `Error: ${ cloneError.message }. Reload to try again.` }/>;
        } else if ( isLoadingCloneItem ) {
            return <InterstitialMessageBlock message="Loading" spinner/>;
        }

        const content = (
            <form onSubmit={ this.handleSubmit }>
                <fieldset disabled={ isSubmitting }>
                    <div className="form-group">
                        <label htmlFor="script-title">Title</label>
                        <input
                            type="text"
                            className="form-control"
                            id="script-title"
                            name="script-title"
                            aria-describedby="script-title-help"
                            placeholder="Enter a title"
                            maxLength={ process.env.REACT_APP_MAX_TITLE_LENGTH }
                        />
                        <small id="script-title-help" className="form-text text-muted">Give your script a title.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="script-description">Description</label>
                        <textarea
                            className="form-control"
                            id="script-description"
                            name="script-description"
                            aria-describedby="script-description-help"
                            placeholder="Enter a description"
                            maxLength={ process.env.REACT_APP_MAX_DESCRIPTION_LENGTH }
                        />
                        <small id="script-description-help" className="form-text text-muted">Briefly describe what your script is supposed to do.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="script-code">Script</label>
                        <ScriptEditor onScriptChange={ this.onScriptChange } item={ cloneItem }/>
                    </div>
                    <button type="submit" className="btn btn-primary">Run</button>
                </fieldset>
            </form>
        );

        return (
            <>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 mb-3">
                    <h1 className="h2">New Script</h1>
                </div>
                { message }
                { content }
            </>
        );
    }
}

export default NewJobView;