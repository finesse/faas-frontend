import React from "react";
import { UnControlled as CodeMirrorPanel } from 'react-codemirror2';
import CodeMirror from 'codemirror';
import 'codemirror/addon/mode/simple.js';
import { InterstitialErrorBlock, InterstitialMessageBlock } from './util';

CodeMirror.defineSimpleMode(
    'kat2',
    {
        // The start state contains the rules that are initially used.
        start: [
            {
                regex: /(ad|attr|beamsplitter[1-2]?|bs[1-2]?|beam|bp|cav|const|dbs|fsig|func|gauss\*?|gnuterm|GNUPLOT|gouy|isol|diode|lens|lock\*?|laser|light|l|mask|maxtem|mod|mirror[1-2]?|m[1-2]?|noplot|noxaxis|pause|phase|pd[0-9]*|pdtype|put\*?|pyterm|qnoised|qshot|retrace|freq|scale|startnode|sq|s|tem|time|variable|x3axis\*?|x2axis\*?|xaxis\*?|yaxis)\b/,
                token: 'keyword',
            },
            {
                regex: /[a-zA-Z_][a-zA-Z0-9_:+-]*\*?/,
                token: 'string',
            },
            {
                regex: /0x[a-f\d]+|[-+]?(?:\.\d+|\d+\.?\d*)(?:e[-+]?\d+)?/i,
                token: 'number',
            },
            {
                regex: /(#|%).*/,
                token: 'comment',
            },
        ],
        // The meta property contains global information about the mode. It
        // can contain properties like lineComment, which are supported by
        // all modes, and also directives like dontIndentStates, which are
        // specific to simple modes.
        meta: {
            dontIndentStates: [
                'comment',
            ],
        }
    }
);

class KatEditor extends React.Component {
    render() {
        const { script = "", readOnly = false, onChange } = this.props;

        const options = {
            lineNumbers: true,
            lineWrapping: true,
            readOnly,
            viewportMargin: Infinity, // Required to allow full code to render.
            mode: 'kat2',
        };

        return (
            <CodeMirrorPanel
                editorDidMount={ editor => { this.editor = editor } }
                value={ script }
                options={ options }
                onChange={ onChange }
            />
        );
    }
}

class ScriptEditor extends React.Component {
    constructor( props ) {
        super( props );

        this.editor = null;

        this.state = {
            error: null,
            isLoading: false,
            script: null,
        };

        // Properly resolve "this" in these methods.
        this.onChange = this.onChange.bind( this );
    }

    componentDidMount() {
        const { item = null, readOnly = false, onScriptChange } = this.props;

        if ( item ) {
            // Fetch existing script.

            this.setState(
                {
                    isLoading: true,
                }
            );

            fetch(
                item.inputKatUrl
            ).then(
                item => item.text()
            ).then(
                ( response ) => {
                    this.setState(
                        {
                            isLoading: false,
                            script: response,
                        }
                    );

                    if ( ! readOnly && onScriptChange ) {
                        // Trigger a script change event so that external states get updated.
                        onScriptChange( response );
                    }
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                ( error ) => {
                    this.setState(
                        {
                            isLoading: false,
                            error
                        }
                    );
                }
            );
        }
    }

    onChange( editor, data, value ) {
        const { onScriptChange } = this.props;

        if ( onScriptChange ) {
            onScriptChange( value );
        }
    }

    render() {
        const { error, isLoading, script } = this.state;
        const { readOnly = false } = this.props;

        if ( error ) {
            let msg = error;

            if ( error instanceof TypeError ) {
                // Override NetworkError message.
                msg = 'The job server cannot be reached right now. Please try again later.';
            }

            return <InterstitialErrorBlock error={ msg }/>;
        } else if ( isLoading ) {
            return <InterstitialMessageBlock message="Loading..." spinner/>;
        }

        return (
            <div className="script">
                <KatEditor script={ script } readOnly={ readOnly } onChange={ this.onChange } { ...this.props }/>
            </div>
        );
    }
}

export { ScriptEditor };